# Overview

[Port forwarding](https://en.wikipedia.org/wiki/Port_forwarding) is necessary for ensuring proper connectivity between two Bitorrent clients ([see here](https://help.bittorrent.com/support/solutions/articles/29000033439-optimizing-your-internet-connection-connection-guide-)). However, many VPN providers disable this to enhance security.  One provider, Private Internet Access, has this capability, but the port is random and must be fetched each time the tunnel is established within 2-minutes.  This becomes a problem when the tunnel dies due to ISP interruptions frequently.

This script fetches an available upload port using Private Internet Access's legacy port forwarding API, and updates the Deluge daemon accordingly using the API. Although in the example below I have deployed it to an Edgerouter Lite 3, it can be used in any context where it is triggered using the OpenVPN `up` or `route-up` configuration lines. While I did find another similar project after completing this script, this project does **not** require that the Deluge daemon and OpenVPN tunnel be running on the same box because the configuration is done via the API.

# Deployment

Currently, to use this script on an Edgemax device (e.g., ERL3), one has to:

1. Enable [Debian Packages](https://help.ui.com/hc/en-us/articles/205202560-EdgeRouter-Add-Debian-Packages-to-EdgeOS) for EdgeOS.
2. Install the [`requests`](https://requests.readthedocs.io/en/master/) and `dotenv` packages: `apt-get install -y --no-install-recommends python-requests python-dotenv`.
3. Use SCP to place the script in a directory on the device (e.g., `/config/scripts/update_deluge.py`).
4. Modify your OpenVPN config file (e.g., *.ovpn) and add:
    ```
    script-security 2
    up /config/scripts/update_deluge.py
    ```

# Further Reading

1. [OpenVPN 2.4 Manual](https://openvpn.net/community-resources/reference-manual-for-openvpn-2-4/). As of firmware 2.0.9, Edgerouter runs OpenVPN 2.4.  This is a great place to start learning about the `up` and `route-up` configuration options.
2. [Deluge API Docs](https://deluge.readthedocs.io/en/latest/reference/webapi.html).
3. [Port Forwarding](https://en.wikipedia.org/wiki/Port_forwarding) article on Wikipedia.
