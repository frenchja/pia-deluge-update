#!/usr/bin/env python

import json
from hashlib import sha256
from os import environ, system, urandom
from sys import exit
from uuid import uuid4
from dotenv import load_dotenv

import requests


load_env()

def log(message):
    # Log using Edgerouter's internal logger
    system('logger -t deluge ' + message)

# Obtain PIA Local IP
pia_ip = environ['ifconfig_local']
log('PIA IP: ' + pia_ip)

# Get port from PIA
r = requests.post('https://www.privateinternetaccess.com/vpninfo/port_forward_assignment',
                  data={'user': environ['PIA_USER'],
                        'pass': environ['PIA_PASS'],
                        'client_id': sha256(urandom(256)).hexdigest(),
                        'local_ip': pia_ip
                        })

data = r.json()

if 'port' in data.keys():
    pia_port = int(data['port'])
    log('PIA Port: ' + data['port'])
else:
    log(data.get('error'))
    # We don't want to prevent the tunnel from being established if the port isn't available
    exit()

# Login to Deluge
s = requests.Session()
DELUGE_URL = 'http://{host}:{port}/json'.format(host=environ['DELUGE_HOST'],
                                                port=environ['DELUGE_PORT'])
try:
    auth = s.post(DELUGE_URL,
                  json = {'id': str(uuid4()),
                          'method':'auth.login',
                          'params': ['deluge']})

    # Set Deluge Port
    r = s.post(DELUGE_URL,
               json = {'id': str(uuid4()),
                       'method':'core.set_config',
                       'params': [{'listen_ports': [pia_port]}]
                       }
               )
except Exception:
    # Do not block tunnel
    exit()

log('Deluge update successful')
