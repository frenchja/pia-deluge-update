.PHONY: notebook
notebook:
	poetry run jupyter notebook

.PHONY: build
build:
	docker-compose build

.PHONY: update
update:
	docker-compose run deluge_update

