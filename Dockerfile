FROM python:2.7

RUN pip install poetry
RUN mkdir -p /app
WORKDIR /app
COPY poetry.lock pyproject.toml /app/
RUN poetry config virtualenvs.create false \
    && poetry install --no-dev


COPY . /app/

CMD ["python", "-m", "deluge_update"]
